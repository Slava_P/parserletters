import json
import os
import comtypes.client
from docxtpl import DocxTemplate
from openpyxl import load_workbook


def get_end(genger):
    if genger == "М":
        return ("ый")
    elif genger == "Ж":
        return ("ая")


def convert_pdf(file, fio):
    wdFormatPDF = 17

    in_file = os.path.abspath(file + ".docx")
    out_file = os.path.abspath("letters/pdf/" + fio + ".pdf")

    word = comtypes.client.CreateObject('Word.Application')
    doc = word.Documents.Open(in_file)
    doc.SaveAs(out_file, FileFormat=wdFormatPDF)
    doc.Close()


# Прасер значений из config.json
config = json.load(open("resources/config.json", "r", encoding="utf8"))

file_path_xlsx = os.getcwd() + config["registries"]["file_path"]
file_path_doc = os.getcwd() + config["letter_template"]["file_path"]
name_work_list = config["registries"]["name_work_list"]
row_count = config["registries"]["rows_count"]

# Открываем для работы файлы реестра и шаблона письма
registry = load_workbook(file_path_xlsx)
letter_template = DocxTemplate(file_path_doc)

# Начинаем работать с каждой строкой файла реестра
for i in range(2, row_count + 1):

    # Достаем из реестра нужные поля
    letter_number = registry[name_work_list]['A' + str(i)].value
    fio = registry[name_work_list]['B' + str(i)].value
    post = registry[name_work_list]['C' + str(i)].value
    fio_to_whom = registry[name_work_list]['D' + str(i)].value
    first_middle_name = registry[name_work_list]['E' + str(i)].value
    gender = registry[name_work_list]['F' + str(i)].value
    end = get_end(gender)

    # Подставляем в вордовский шаблон
    context = {
        'letter_number': letter_number,
        'post': post,
        'fio_to_whom': fio_to_whom,
        'end': end,
        'first_middle_name': first_middle_name
    }
    letter_template.render(context)

    # Сохраняем полученное письмо в формате .docx
    letter_template.save("letters/Письмо-приглашение " + fio_to_whom + ".docx")

    # Сохраненное письмо в формате .docx конвентируем в формат .pdf
    convert_pdf("letters/Письмо-приглашение " + fio_to_whom, fio)
    print("Создано ", i - 1, " письмо")
